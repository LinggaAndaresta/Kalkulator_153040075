/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kalkulator_153040075;

import java.util.Scanner;

/**
 *
 * @author SB601-40
 */
public class Kalkulator_153040075 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        double penjumlahan;
        double pengurangan;
        double perkalian;
        double pembagian;
        double hasil = 0;
        float bil1 = 0;
        float bil2 = 0;

        Scanner sc = new Scanner(System.in);

        System.out.print("Masukan Bilangan Ke-1 = ");
        bil1 = sc.nextFloat();
        System.out.print("Masukan Bilangan Ke-2 = ");
        bil2 = sc.nextFloat();

        System.out.println("Penjumlahan");
        penjumlahan = bil1 + bil2;
        System.out.println("Hasil Penjumlahan = " + penjumlahan);

        System.out.println("Pengkurangan");
        pengurangan = bil1 - bil2;
        System.out.println("Hasil Pengurangan = " + pengurangan);

        System.out.println("Perkalian");
        perkalian = bil1 * bil2;
        System.out.println("Hasil Perkalian = " + perkalian);

        System.out.println("Pembagian");
        pembagian = bil1 / bil2;
        System.out.println("Hasil Pembagian = " + pembagian);

    }
}
